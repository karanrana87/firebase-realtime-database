package com.arq.firebasedemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arq.firebasedemo.model.Message
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_main.*

class PremiumChatGroupActivity : AppCompatActivity() {
    val REQUEST_SELECT_IMAGE_IN_ALBUM: Int = 888
    val mObservableList = ObservableArrayList<Message>()
    val RC_SIGN_IN = 999
    var mUserName: String? = null
    lateinit var adapter: MessagesAdapter

    // Listener for monitoring the change in the authentication state
    lateinit var authStateListener: AuthStateListener

    // Entry point of the Firebase authentication SDK
    lateinit var firebaseAuth: FirebaseAuth

    lateinit var firebasePremiumDBInstance: DatabaseReference

    // Entry point of Firebase Storage SDK
    lateinit var firebaseStorage: FirebaseStorage

    lateinit var firebaseStorageReference: StorageReference
    var childEventListener: ChildEventListener? = null

    // Choose authentication providers
    // Make sure to enable the providers in the Firebase console
    val providers = arrayListOf(
        AuthUI.IdpConfig.EmailBuilder().build(),
        AuthUI.IdpConfig.GoogleBuilder().build(),
        AuthUI.IdpConfig.PhoneBuilder().build()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_premium_main)
        val firebaseInstance = FirebaseDatabase.getInstance()
        firebaseStorage = FirebaseStorage.getInstance()

        // "PREMIUM-CHAT-IMAGES" is the bucket name in Firebase Console where we are saving our images
        firebaseStorageReference = firebaseStorage.getReference("PREMIUM-CHAT-IMAGES")

        // "premium" is the root of our messages db
        firebasePremiumDBInstance = firebaseInstance.getReference("premium")

        firebaseAuth = FirebaseAuth.getInstance()

        send_message.setOnClickListener({ v ->
            firebasePremiumDBInstance.push()
                .setValue(Message(editText.text.toString(), mUserName))
        })

        image_picker.visibility = View.VISIBLE
        image_picker.setOnClickListener({ v ->
            selectImageInAlbum();
        })

        authStateListener = object : AuthStateListener {
            /*
            This method gets invoked in the UI thread on changes in the authentication state:
            Right after the listener has been registered
            When a user is signed in
            When the current user is signed out
            */
            override fun onAuthStateChanged(p0: FirebaseAuth) {
                // If the user is not logged in current user will be null
                // and we launch the FireBase Sign In Page
                if (p0.currentUser == null) {
                    startActivityForResult(
                        AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .setIsSmartLockEnabled(false)
                            .build(),
                        RC_SIGN_IN
                    )
                } else {
                    //Username saved for future use
                    mUserName = p0.currentUser!!.displayName
                    childEventListener = object : ChildEventListener {

                        override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                        }

                        override fun onCancelled(p0: DatabaseError) {
                            Log.e("","ON ERROR "+ p0)
                        }

                        override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {
                        }

                        override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                        }

                        override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                            // Add the new message to the list
                            var msg = dataSnapshot.getValue(Message::class.java)
                            msg?.let { mObservableList.add(it) }
                            messages.scrollToPosition(mObservableList.size - 1)
                        }

                    }
                    //User is Logged In and we add the child Event Listener to Read the messages
                    firebasePremiumDBInstance.addChildEventListener(childEventListener as ChildEventListener)
                }
            }
        }
        setAdapter()
    }

    fun setAdapter() {
        adapter = MessagesAdapter(mObservableList)
        messages.setHasFixedSize(true);
        messages.setItemViewCacheSize(20);
        messages.setDrawingCacheEnabled(true);
        messages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        messages.adapter = adapter
        messages.addItemDecoration(DividerItemDecoration(this, 0))
        messages.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
    }

    override fun onResume() {
        super.onResume()
        // Register the AuthState Listener
        firebaseAuth.addAuthStateListener(authStateListener)
    }

    override fun onPause() {
        super.onPause()
        // Remove any registered listeners
        firebaseAuth.removeAuthStateListener(authStateListener)
        childEventListener?.let { firebasePremiumDBInstance.removeEventListener(it) }
        mUserName = null
        mObservableList.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
            } else if (resultCode == Activity.RESULT_CANCELED) {
                finish()
            }
        } else if (requestCode == REQUEST_SELECT_IMAGE_IN_ALBUM) {
            if (resultCode == Activity.RESULT_OK) {
                var imageURI = data?.data
                var ref = imageURI?.lastPathSegment?.let { firebaseStorageReference.child(it) }
                var task = imageURI?.let { ref?.putFile(it) }
                task?.addOnSuccessListener { taskSnapshot: UploadTask.TaskSnapshot? ->
                    ref?.downloadUrl?.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val downloadUri = task.result
                            val downloadURL = downloadUri.toString()
                            firebasePremiumDBInstance.push()
                                .setValue(Message("", mUserName, downloadURL))
                        }
                    }

                }
            }
        }
    }

    fun selectImageInAlbum() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
        }
    }
}


