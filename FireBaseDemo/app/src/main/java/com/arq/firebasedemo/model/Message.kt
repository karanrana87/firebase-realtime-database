package com.arq.firebasedemo.model

data class Message(
    var text: String = "", var sender: String? = "Anonymous", var imageURI: String = ""
)