package com.arq.firebasedemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.arq.firebasedemo.model.Message
import com.arq.firebasedemo.model.User
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val mObservableList = ObservableArrayList<Message>()
    val RC_SIGN_IN = 999
    var mUserName: String? = null
    var mUID: String? = null
    lateinit var adapter: MessagesAdapter
    lateinit var authStateListener: AuthStateListener
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var firebaseDBInstance: DatabaseReference
    lateinit var firebaseUserDBInstance: DatabaseReference
    lateinit var firebaseStorage: FirebaseStorage
    var childEventListener: ChildEventListener? = null
    // Choose authentication providers
    val providers = arrayListOf(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.PhoneBuilder().build()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firebaseInstance = FirebaseDatabase.getInstance()
        firebaseStorage = FirebaseStorage.getInstance()
        firebaseDBInstance = firebaseInstance.getReference("basic")
        firebaseUserDBInstance = firebaseInstance.getReference("users")

        firebaseAuth = FirebaseAuth.getInstance()

        send_message.setOnClickListener({ v ->
            firebaseDBInstance.push()
                    .setValue(Message(editText.text.toString(), mUserName))
            editText.setText("")
        })
        authStateListener = object : AuthStateListener {
            /*
            This method gets invoked in the UI thread on changes in the authentication state:
            Right after the listener has been registered
            When a user is signed in
            When the current user is signed out
            */
            override fun onAuthStateChanged(p0: FirebaseAuth) {

                // If the user is not logged in current user will be null
                // and we launch the FireBase Sign In Page
                if (p0.currentUser == null) {
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setAvailableProviders(providers)
                                    .setIsSmartLockEnabled(false)
                                    .build(),
                            RC_SIGN_IN
                    )
                } else {
                    //Username saved for future use
                    mUserName = p0.currentUser!!.displayName
                    mUID = p0.currentUser!!.uid
                    //User object updated in Firebase Storage
                    firebaseUserDBInstance.addListenerForSingleValueEvent(
                            object : ValueEventListener {
                                override fun onCancelled(p0: DatabaseError) {
                                }

                                override fun onDataChange(p0: DataSnapshot) {
                                    val users = p0.value
                                    if (users != null) {
                                        users as HashMap<String, User>
                                        if (!users.containsKey(mUID as String)) {
                                            users.put(mUID!!, User(false, mUserName!!))
                                            firebaseUserDBInstance.setValue(users)
                                        }
                                    }
                                }
                            })
                    childEventListener =
                            object : ChildEventListener {
                                override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                                }

                                override fun onCancelled(p0: DatabaseError) {
                                }

                                override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {
                                }

                                override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                                }

                                override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                                    var msg = dataSnapshot.getValue(Message::class.java)
                                    msg?.let { mObservableList.add(it) }
                                    messages.scrollToPosition(mObservableList.size - 1)
                                }
                            }
                    //User is Logged In and we add the child Event Listener to Read the messages
                    firebaseDBInstance.addChildEventListener(childEventListener as ChildEventListener)
                }
            }
        }
        setAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sign_out -> {
                AuthUI.getInstance().signOut(this)
                return true
            }
            R.id.premium -> {
                val users = HashMap<String, User>()
                users.put(mUID!!, User(true, mUserName!!))
                firebaseUserDBInstance.updateChildren(users as Map<String, Any>)
                return true
            }
            R.id.premium_chat_group -> {
                startActivity(Intent(this, PremiumChatGroupActivity::class.java))
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setAdapter() {
        adapter = MessagesAdapter(mObservableList)
        messages.setHasFixedSize(true);
        messages.setItemViewCacheSize(20);
        messages.setDrawingCacheEnabled(true);
        messages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        messages.adapter = adapter
        messages.addItemDecoration(DividerItemDecoration(this, 0))
        messages.layoutManager = LinearLayoutManager(this)
    }

    override fun onResume() {
        super.onResume()
        firebaseAuth.addAuthStateListener(authStateListener)
    }

    override fun onPause() {
        super.onPause()
        firebaseAuth.removeAuthStateListener(authStateListener)
        childEventListener?.let { firebaseDBInstance.removeEventListener(it) }
        mUserName = null
        mObservableList.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_CANCELED) {
                finish()
            }
        }
    }
}


