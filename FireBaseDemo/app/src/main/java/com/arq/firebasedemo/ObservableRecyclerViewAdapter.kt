package com.arq.firebasedemo

import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView

/**
 * ObservableRecyclerViewAdapter is used to allow changes to ObservableList to update the RecyclerView
 */
abstract class ObservableRecyclerViewAdapter<T, Holder: RecyclerView.ViewHolder>(
    private val rowItems: ObservableList<T>) : RecyclerView.Adapter<Holder>() {

  init {
    rowItems.addOnListChangedCallback(object: ObservableList.OnListChangedCallback<ObservableList<T>>() {
      override fun onChanged(sender: ObservableList<T>?) {
        notifyDataSetChanged()
      }

      override fun onItemRangeRemoved(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
        notifyItemRangeRemoved(positionStart, itemCount)
      }

      override fun onItemRangeMoved(sender: ObservableList<T>?, fromPosition: Int, toPosition: Int, itemCount: Int) {
        for (i in 0 until itemCount) {
          notifyItemMoved(fromPosition + i, toPosition + i)
        }
      }

      override fun onItemRangeInserted(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
        notifyItemRangeInserted(positionStart, itemCount)
      }

      override fun onItemRangeChanged(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
        notifyItemRangeChanged(positionStart, itemCount)
      }
    })
    setHasStableIds(true)
  }

  override fun getItemCount(): Int = rowItems.size

  fun getItem(i: Int): T {
    return rowItems[i]
  }

  fun getItems(): ObservableList<T> {
    return rowItems
  }

  override fun getItemId(position: Int): Long {
    return rowItems[position].hashCode().toLong()
  }
}
