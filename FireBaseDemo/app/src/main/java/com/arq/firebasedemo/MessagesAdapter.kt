package com.arq.firebasedemo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.arq.firebasedemo.databinding.MessageRecyclerViewItemMainBinding
import com.arq.firebasedemo.model.Message
import com.squareup.picasso.Picasso

class MessagesAdapter(messages: ObservableList<Message>) :
    ObservableRecyclerViewAdapter<Message, MessagesAdapter.Holder>(messages) {
    var messages: ObservableList<Message>

    init {
        this.messages = messages
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val inflater = LayoutInflater.from(parent.context)

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            inflater,
            R.layout.message_recycler_view_item_main, parent, false
        )
        return Holder(
            binding as MessageRecyclerViewItemMainBinding
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemCount(): Int {
        return if (messages != null) {
            messages.size
        } else {
            0
        }
    }

    inner class Holder(private val binding: MessageRecyclerViewItemMainBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var messagObject: Message? = null

        fun bind(message: Message) {
            this.messagObject = message
            if (messagObject!!.imageURI.isEmpty()) {
                binding.chatImage.visibility = View.GONE
                binding.tvTitle.visibility = View.VISIBLE
                binding.tvTitle.text = messagObject!!.text
            } else {
                binding.tvTitle.visibility = View.GONE
                binding.chatImage.visibility = View.VISIBLE
                Picasso.with(binding.chatImage.context).load(messagObject!!.imageURI)
                    .into(binding.chatImage);
            }
            binding.subTitle.text = messagObject!!.sender
        }
    }
}