package com.arq.firebasedemo.model

data class User(
    var isPaid: Boolean = false, var userName: String = ""
)